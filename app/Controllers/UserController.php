<?php

class UserController extends Controller
{
	private $user;

	public function __construct()
	{
		$this->user = $this->Model('User');
	}

	public function index()
	{	
		$this->user->setName('Oscar');
		$info_user = ['user' => $this->user];
		return $this->View('user/index', $info_user);
	}

	public function action($name = 'Andres')
	{
		return $this->View('user/edit');
	}

}