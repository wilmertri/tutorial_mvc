<?php

class App {

	public $controller = 'Home';

	public $action = 'index';

	public $params = [];

	public function __construct()
	{
		$url = $this->parseUrl();

		if (file_exists('../app/Controllers/' . $url[0] . 'Controller.php')) 
		{
			$this->controller = $url[0];
			unset($url[0]);
		}

		require_once '../app/Controllers/' . $this->controller . 'Controller.php';

		$this->controller = $this->controller . 'Controller';

		$this->controller = new $this->controller;

		if (isset($url[1])) 
		{
			if (method_exists($this->controller, $url[1])) 
			{
				$this->action = $url[1];
				unset($url[1]);
			}
		}

		$this->params = $url ? array_values($url) : [];

		call_user_func_array([$this->controller, $this->action], $this->params);
	}

	public function parseUrl()
	{
		if(isset($_GET['url']))
		{
			return $url = explode('/' ,filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL));
		}
	}
}