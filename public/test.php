<?php

class Clase1
{
	public function method1()
	{
		echo "Es el número 1";
	}
}

class Clase2 extends Clase1
{
	public function method2()
	{
		echo "Es el número 2";
	}

	public function method3()
	{
		$this->method1();
	}
}

$c2 = new Clase2();
var_dump($c2);
$c2->method3();